
# INSERT INTO restaurant (id, name, address, image_url) VALUES (1, "Puberaj", "Novi Sad", 'http://localhost:8080/1.jpg');
# INSERT INTO restaurant (id, name, address, image_url) VALUES (2, "Irish Pub", "Novi Sad", 'http://localhost:8080/2.jpg');
INSERT INTO user(id, email, password,role,username) VALUES (1, "modelator@m", '$2a$12$DCvOp372tDO89FjTiYS1t.NBcYsq3Bj1jqx3WPoFeFCnj6EPJcmoG', 'MODELATOR', 'modelator')
INSERT INTO user(id, email, password,role,username) VALUES (2, "daca@d", '$2a$12$HbniNtQELQ9LWfmPCQgVFuc7E4cRG3BzkIjSYcrcq7RS.XGcmABxG', 'USER', 'daca');

INSERT INTO cart(id, total_price, user_id) VALUES (1, 0, 1);
INSERT INTO cart(id, total_price, user_id) VALUES (2, 0, 2);
# INSERT INTO product (id, name, price, restaurant_id) VALUES (1, 'Index', 580.00, 1);
# INSERT INTO product (id, name, price, restaurant_id) VALUES (2, 'Club sendvic', 700.00, 1);
# INSERT INTO product (id, name, price, restaurant_id) VALUES (3, 'Potaz', 250.00, 1);
# INSERT INTO product (id, name, price, restaurant_id) VALUES (4, 'Piletina u krem sosu', 940.00, 1);
# INSERT INTO product (id, name, price, restaurant_id) VALUES (5, 'Pohovani batak', 820.00, 1);


INSERT INTO product (id, name, price, category, description) VALUES (1, 'Grill ham & cheese tost', 510.00, 0, 'Mlečni tost, trapist, šunka, mix salata, majonez, pomfrit');

INSERT INTO product (id, name, price, category, description) VALUES (2, 'Index', 580.00, 0, 'Brioche kifla, kuvana šunka, trapist, šampinjoni sa začinima, mix salata, pavlaka, majonez, pomfrit');
INSERT INTO product (id, name, price, category, description) VALUES (3, 'Club sendvic', 770.00, 0, 'Mlečni tost, piletina, slanina, trapist, čedar sos, mustard majonez, remulad sos, pomfrit, mix salata');
INSERT INTO product (id, name, price, category, description) VALUES (4, 'Kobaja sir', 630.00, 0, 'Brioche kifla, kobaja sa sirom, coleslaw, senf-med, kiseli krastavci, hrskavi luk, pomfrit');
INSERT INTO product (id, name, price, category, description) VALUES (5, 'Pohovana uteha', 650.00, 0, 'Brioche kifla, pohovana piletina, coleslaw salata, spicy mayo, kiseli krastavci, pomfrit');

INSERT INTO product (id, name, price, category, description) VALUES (11, 'Popaj', 770.00, 1, 'Brioche bun, junetina 150g, spanać, čeri paradajz, čedar sir, zelena salata, crveni luk, beli sos, pomfrit');
INSERT INTO product (id, name, price, category, description) VALUES (12, 'Cheeseburger', 820.00, 1, 'Brioche bun, junetina 150g, čedar sir, pohovani trapist, iceberg, kiseli krastavci, pomfrit');
INSERT INTO product (id, name, price, category, description) VALUES (13, 'Burgija by Angelo', 890.00, 1, 'Brioche bun, junetina 150g, zelena salata, paladin sir, slaninica, sos od šampinjona, čedar sir, paradajz, jalapenjo kisele paprike, crveni luk, crveni sos, pomfrit');
INSERT INTO product (id, name, price, category, description) VALUES (14, 'Truffle burger', 820.00, 1, 'Brioche bun, junetina 150g, slanina, cheddar sir, paradajz, kiseli krasavci, iceberg, truffle mayo, senf, pomfrit');

INSERT INTO product (id, name, price, category, description) VALUES (6, 'Pork steak', 990.00, 2, 'Dugo marinirani svinjski vrat, grilovano povće,mladi krompir, salatica');
INSERT INTO product (id, name, price, category, description) VALUES (7, 'Piletina u krem sosu', 940.00, 2, 'Sočna piletina, šampinjoni, sveža paprika, bukovača, neutralna pavlaka, pire krompir, parmezan, senf, salatica');
INSERT INTO product (id, name, price, category, description) VALUES (8, 'Pohovani batak', 820.00, 2, 'Pohovani batak bez kostiju, pire krompir, grilovano povrće, salatica');
INSERT INTO product (id, name, price, category, description) VALUES (9, 'Pilece belo', 870.00, 2, 'Marinirano belo meso, pire kromppir, grilovano povrće, salatica');
INSERT INTO product (id, name, price, category, description) VALUES (10, 'Dvosed', 2160.00, 2, 'Svinjski vrat, pileći batak, slaninica, kobaja sa sirom, mladi krompir, grilovano povrće, demi-glace sos, hrskavi hleb, salatica');
INSERT INTO product (id, name, price, category, description) VALUES (15, 'Zelena pijaca aka vege riza', 750.00, 2, 'Pirinac, tikvice, praziluk, sampinjoni, spanac, sargarepa. hrskavi crni luk, kikiriki');
INSERT INTO product (id, name, price, category, description) VALUES (16, 'Becka vege', 790.00, 2, 'Vege snicla, grilovano povrce, krompir salata, cherry paradajz, posni tartar');


