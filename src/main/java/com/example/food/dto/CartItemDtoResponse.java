package com.example.food.dto;

public class CartItemDtoResponse {

    private long id;
    private int quantity;
    private long productId;
    private double price;
    private String productName;

    public CartItemDtoResponse(long id, int quantity, long productId, double price, String productName) {
        this.id = id;
        this.quantity = quantity;
        this.productId = productId;
        this.price = price;
        this.productName = productName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }
}
