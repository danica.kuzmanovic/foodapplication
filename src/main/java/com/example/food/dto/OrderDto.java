package com.example.food.dto;


import java.util.List;

public class OrderDto {

    private int id;
    private String address;
    private double price;
    private String stage;
    private List<OrderItemDto> orderItemList;

    public OrderDto(int id, String address, double price, String stage, List<OrderItemDto> orderItemList) {
        this.id = id;
        this.address = address;
        this.price = price;
        this.stage = stage;
        this.orderItemList = orderItemList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public List<OrderItemDto> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OrderItemDto> orderItemList) {
        this.orderItemList = orderItemList;
    }
}
