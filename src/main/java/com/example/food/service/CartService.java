package com.example.food.service;

import com.example.food.repository.model.Cart;

public interface CartService {

    Cart findByUserId(Long id);

    Cart save(Cart cart);

    void update(Cart cart);


}
