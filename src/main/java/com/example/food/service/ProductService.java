package com.example.food.service;

import com.example.food.repository.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    Optional<Product> findById(long id);

    List<Product> findAll();
}
