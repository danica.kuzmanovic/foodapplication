package com.example.food.service;

import com.example.food.repository.model.Order;
import com.example.food.repository.model.OrderItem;

import java.util.List;


public interface OrderService {

    void add(List<OrderItem> orderItemList);

    void save(Order order);

    List<Order> findAll();

    List<OrderItem> findAllByOrderId(int id);

    Order findById(int id);

    void delete(Order order);

    void deleteOrderItems(List<OrderItem> id);

    List<Order> findByUserId(long id);
}
