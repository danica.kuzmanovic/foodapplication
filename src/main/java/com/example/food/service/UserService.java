package com.example.food.service;

import com.example.food.repository.model.User;

public interface UserService {

    User findbyUsername(String username);

    void save(User user);
}
