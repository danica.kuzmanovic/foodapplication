package com.example.food.service.serviceImpl;


import com.example.food.repository.UserRepository;
import com.example.food.repository.model.User;
import com.example.food.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserDetailsImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User findbyUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }
}
