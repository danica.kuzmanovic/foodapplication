package com.example.food.service.serviceImpl;

import com.example.food.repository.CartRepository;
import com.example.food.repository.model.Cart;
import com.example.food.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    CartRepository cartRepository;

    @Override
    public Cart findByUserId(Long id) {
        return cartRepository.findByUserId(id);
    }

    @Override
    public Cart save(Cart cart) {
        return cartRepository.save(cart);
    }

    @Override
    public void update(Cart cart) {
        cartRepository.save(cart);
    }
}
