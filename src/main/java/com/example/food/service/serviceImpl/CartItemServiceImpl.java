package com.example.food.service.serviceImpl;

import com.example.food.repository.CartItemRepository;
import com.example.food.repository.model.CartItem;
import com.example.food.service.CartItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CartItemServiceImpl implements CartItemService {

    @Autowired
    CartItemRepository cartItemRepository;

    @Override
    public void save(CartItem cartItem) {
        cartItemRepository.save(cartItem);
    }

    @Override
    public CartItem findByProductIdAndCartId(long id, long id1) {
        return cartItemRepository.findByProductIdAndCartId(id, id1);
    }

    @Override
    public List<CartItem> findByCartId(long cartId) {
        return cartItemRepository.findAllByCartId(cartId);
    }

    @Override
    public void delete(CartItem cartItem) {
        cartItemRepository.delete(cartItem);
    }

    @Override
    public Optional<CartItem> findById(int id) {
        return cartItemRepository.findById(id);
    }

    @Override
    public void deleteAll(List<CartItem> cartItemList) {
        cartItemRepository.deleteAll(cartItemList);
    }
}
