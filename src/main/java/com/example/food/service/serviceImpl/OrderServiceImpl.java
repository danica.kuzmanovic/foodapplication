package com.example.food.service.serviceImpl;

import com.example.food.repository.OrderItemRepository;
import com.example.food.repository.OrderRepository;
import com.example.food.repository.model.Order;
import com.example.food.repository.model.OrderItem;
import com.example.food.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderItemRepository orderItemRepository;

    @Autowired
    OrderRepository orderRepository;

    @Override
    public void add(List<OrderItem> orderItemList) {
        orderItemRepository.saveAll(orderItemList);
    }

    @Override
    public void save(Order order) {
        orderRepository.save(order);
    }

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public List<OrderItem> findAllByOrderId(int id) {
        return orderItemRepository.findAllByOrderId(id);
    }

    @Override
    public Order findById(int id) {
        return orderRepository.getOne(id);
    }

    @Override
    public void delete(Order order) {
        orderRepository.delete(order);
    }

    @Override
    public void deleteOrderItems(List<OrderItem> id) {
        orderItemRepository.deleteAll(id);
    }

    @Override
    public List<Order> findByUserId(long id) {
        return orderRepository.findAllByUserId(id);
    }
}
