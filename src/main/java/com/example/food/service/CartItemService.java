package com.example.food.service;

import com.example.food.repository.model.CartItem;

import java.util.List;
import java.util.Optional;

public interface CartItemService {

    void save(CartItem cartItem);

    CartItem findByProductIdAndCartId(long id, long id1);

    List<CartItem> findByCartId(long cartId);

    void delete(CartItem id);

    Optional<CartItem> findById(int id);

    void deleteAll(List<CartItem> cartItemList);
}
