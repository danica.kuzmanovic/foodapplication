package com.example.food.repository;

import com.example.food.repository.model.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartItemRepository extends JpaRepository<CartItem, Integer> {

    List<CartItem> findAllByCartId(long cartId);

    CartItem findByProductIdAndCartId(long id, long id1);
}
