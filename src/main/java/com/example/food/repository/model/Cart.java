package com.example.food.repository.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private double totalPrice = 0;

    @OneToOne
    private User user;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cart", fetch = FetchType.LAZY)
    private List<CartItem> cardItems = new ArrayList<>();

    public Cart() {
    }

    public Cart(long id, double totalPrice, User user) {
        this.id = id;
        this.totalPrice = totalPrice;
        this.user = user;
    }

    public Cart(User user) {
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<CartItem> getCardItems() {
        return cardItems;
    }

    public void setCardItems(List<CartItem> cardItems) {
        this.cardItems = cardItems;
    }
}
