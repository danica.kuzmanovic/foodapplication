package com.example.food.repository.model;

public enum UserRole {
    USER,
    ADMIN,
    MODELATOR
}
