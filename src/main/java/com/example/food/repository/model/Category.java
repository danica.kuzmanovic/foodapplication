package com.example.food.repository.model;


public enum Category {
    SANDWICH,
    BURGER,
    MAIN_DISH
}
