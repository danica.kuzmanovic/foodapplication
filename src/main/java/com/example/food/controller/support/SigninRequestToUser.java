package com.example.food.controller.support;


import com.example.food.dto.request.SignupRequest;
import com.example.food.repository.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;



@Component
public class SigninRequestToUser implements Converter<SignupRequest, User> {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User convert(SignupRequest signupRequest) {
        String encodedPassword = passwordEncoder.encode(signupRequest.getPassword());
        return new User(signupRequest.getUsername(), signupRequest.getEmail(), encodedPassword);
    }
}
