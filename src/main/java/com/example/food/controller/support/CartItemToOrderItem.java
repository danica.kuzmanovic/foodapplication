package com.example.food.controller.support;

import com.example.food.repository.model.CartItem;
import com.example.food.repository.model.Order;
import com.example.food.repository.model.OrderItem;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CartItemToOrderItem {

    public OrderItem convert(CartItem cartItem, Order order) {
        return new OrderItem(cartItem.getQuantity(), cartItem.getProduct(), cartItem.getTotalPrice(), order);
    }

    public List<OrderItem> convert(List<CartItem> cartItemList, Order order) {
        List<OrderItem> orderItemList = cartItemList.stream().map(cartItem -> convert(cartItem, order)).collect(Collectors.toList());
        return orderItemList;
    }
}
