package com.example.food.controller.support;

import com.example.food.dto.OrderDto;
import com.example.food.dto.OrderItemDto;
import com.example.food.repository.model.Order;
import com.example.food.repository.model.OrderItem;
import com.example.food.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderToOrderDto {

    @Autowired
    OrderService orderService;

    public OrderDto convert(Order order) {
        List<OrderItem> orderItems = orderService.findAllByOrderId(order.getId());
        List<OrderItemDto> orderItemDtoList = orderItems.stream().map(orderItem ->
                new OrderItemDto(orderItem.getProduct().getName(),
                        orderItem.getQuantity())).collect(Collectors.toList());
        double price = orderItems.stream().mapToDouble(orderItem -> orderItem.getPrice()).sum();
        return new OrderDto(order.getId(), order.getAddress(), price, order.getStage(), orderItemDtoList);
    }

    public List<OrderDto> convert(List<Order> orders) {
        List<OrderDto> orderDtoList = new ArrayList<>();
        orders.forEach(order -> orderDtoList.add(convert(order)));
        return orderDtoList;
    }
}
