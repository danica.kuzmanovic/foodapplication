package com.example.food.controller.support;

import com.example.food.dto.ProductDto;
import com.example.food.repository.model.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductToProductDto {

    public ProductDto convert(Product product) {
        return new ProductDto(product.getId(), product.getName(), product.getPrice(), product.getDescription(), product.getCategory().ordinal());
    }

    public List<ProductDto> convert(List<Product> products) {
        List<ProductDto> productDtoList = products.stream().map(product -> convert(product)).collect(Collectors.toList());
        return productDtoList;
    }
}
