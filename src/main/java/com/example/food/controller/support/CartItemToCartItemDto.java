package com.example.food.controller.support;

import com.example.food.dto.CartItemDtoResponse;
import com.example.food.repository.model.CartItem;
import com.example.food.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CartItemToCartItemDto {

    @Autowired
    ProductService productService;

    public CartItemDtoResponse convert(CartItem cartItem) {
        return new CartItemDtoResponse(cartItem.getId(), cartItem.getQuantity(), cartItem.getProduct().getId(),
                cartItem.getProduct().getPrice(), cartItem.getProduct().getName());
    }

    public List<CartItemDtoResponse> convert(List<CartItem> cartItemList) {
        List<CartItemDtoResponse> cartItemDtoResponseList = cartItemList.stream().map(cartItem -> convert(cartItem)).collect(Collectors.toList());
        return cartItemDtoResponseList;
    }
}
