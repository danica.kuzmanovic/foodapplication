package com.example.food.controller;

import com.example.food.controller.support.ProductToProductDto;
import com.example.food.dto.ProductDto;
import com.example.food.repository.model.Product;
import com.example.food.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @Autowired
    ProductToProductDto toProductDto;

    @GetMapping
    public ResponseEntity<List<ProductDto>> getProducts() {
        List<Product> productList = productService.findAll();
        List<ProductDto> productDtoList = toProductDto.convert(productList);
        return new ResponseEntity<>(productDtoList, HttpStatus.OK);
    }
}
