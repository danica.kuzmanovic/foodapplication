package com.example.food.controller;

import com.example.food.config.TokenUtils;
import com.example.food.controller.support.CartItemToCartItemDto;
import com.example.food.dto.CartItemDto;
import com.example.food.dto.CartItemDtoResponse;
import com.example.food.repository.CartRepository;
import com.example.food.repository.model.Cart;
import com.example.food.repository.model.CartItem;
import com.example.food.repository.model.Product;
import com.example.food.repository.model.User;
import com.example.food.service.CartItemService;
import com.example.food.service.CartService;
import com.example.food.service.ProductService;
import com.example.food.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    TokenUtils tokenUtils;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    @Autowired
    CartService cartService;

    @Autowired
    CartItemService cartItemService;

    @Autowired
    CartRepository cartRepository;

    @Autowired
    CartItemToCartItemDto toCartItemDto;

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/items")
    public ResponseEntity<List<CartItemDtoResponse>> getCartItems(@RequestHeader("Authorization") String token) {
        token = token.substring(7);
        String username = tokenUtils.getUsernameFromToken(token);
        User user = userService.findbyUsername(username);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Cart cart = cartService.findByUserId(user.getId());
        List<CartItem> cartItemList = cartItemService.findByCartId(cart.getId());
        List<CartItemDtoResponse> cartItemDtoList = toCartItemDto.convert(cartItemList);

        return new ResponseEntity<>(cartItemDtoList, HttpStatus.OK);
    }

    @GetMapping
    public List<Cart> getAllItem() {
        return cartRepository.findAll();
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/addCartItem")
    public ResponseEntity addCartItem(@RequestHeader("Authorization") String token, @RequestBody CartItemDto cartItemDto) {
        token = token.substring(7);
        String username = tokenUtils.getUsernameFromToken(token);
        User user = userService.findbyUsername(username);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Optional<Product> product = productService.findById(cartItemDto.getId());
        if (product.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Cart cart = cartService.findByUserId(user.getId());
        if (cart == null) {
            cart = cartService.save(new Cart(user));
        }
        CartItem cartItem = cartItemService.findByProductIdAndCartId(cartItemDto.getId(), cart.getId());

        if (cartItem == null) {
            cartItem = new CartItem(cartItemDto.getQuantity(), cartItemDto.getQuantity() * product.get().getPrice(), cart, product.get());
        } else {
            cartItem.setQuantity(cartItemDto.getQuantity());
            cartItem.setTotalPrice(cartItemDto.getQuantity() * product.get().getPrice());
        }
        cartItemService.save(cartItem);
        cart.setTotalPrice(cart.getTotalPrice());
        cartService.update(cart);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @DeleteMapping("/{id}")
    public ResponseEntity deleteCartItem(@RequestHeader("Authorization") String token, @PathVariable int id) {
        token = token.substring(7);
        String username = tokenUtils.getUsernameFromToken(token);
        User user = userService.findbyUsername(username);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Optional<CartItem> cartItem = cartItemService.findById(id);
        if (cartItem.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        cartItemService.delete(cartItem.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
