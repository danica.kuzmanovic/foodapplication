package com.example.food.controller;

import com.example.food.config.TokenUtils;
import com.example.food.controller.support.CartItemToOrderItem;
import com.example.food.controller.support.OrderToOrderDto;
import com.example.food.dto.AddressDto;
import com.example.food.dto.OrderDto;
import com.example.food.repository.model.*;
import com.example.food.service.CartItemService;
import com.example.food.service.CartService;
import com.example.food.service.OrderService;
import com.example.food.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    TokenUtils tokenUtils;

    @Autowired
    UserService userService;

    @Autowired
    CartItemService cartItemService;

    @Autowired
    CartService cartService;

    @Autowired
    CartItemToOrderItem cartItemToOrderItem;

    @Autowired
    OrderService orderService;

    @Autowired
    OrderToOrderDto toOrderDto;

    @PreAuthorize("hasRole('ROLE_MODELATOR')")
    @GetMapping
    public ResponseEntity getAllOrders() {
        List<Order> orders = orderService.findAll();
        List<OrderDto> orderDtoList = toOrderDto.convert(orders);
        return new ResponseEntity<>(orderDtoList, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/user")
    public ResponseEntity getOrdersForUser(@RequestHeader("Authorization") String token) {
        token = token.substring(7);
        String username = tokenUtils.getUsernameFromToken(token);
        User user = userService.findbyUsername(username);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        List<Order> orders = orderService.findByUserId(user.getId());
        List<OrderDto> orderDtoList = toOrderDto.convert(orders);
        return new ResponseEntity<>(orderDtoList, HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping
    public ResponseEntity addOrder(@RequestHeader("Authorization") String token, @RequestBody AddressDto address) {

        token = token.substring(7);
        String username = tokenUtils.getUsernameFromToken(token);
        User user = userService.findbyUsername(username);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        Cart cart = cartService.findByUserId(user.getId());
        List<CartItem> cartItemList = cartItemService.findByCartId(cart.getId());

        Order order = new Order(address.getAddress(), user);
        orderService.save(order);
        List<OrderItem> orderItemList = cartItemToOrderItem.convert(cartItemList, order);
        orderService.add(orderItemList);
        cartItemService.deleteAll(cartItemList);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_MODELATOR')")
    @PostMapping("/{id}")
    public ResponseEntity updateOrder(@PathVariable int id) {

        Order order = orderService.findById(id);
        if (order == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        order.setStage("ready");
        orderService.save(order);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_MODELATOR')")
    @DeleteMapping("/{id}")
    public ResponseEntity deleteOrder(@PathVariable int id) {

        Order order = orderService.findById(id);
        if (order == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        List<OrderItem> orderItemList = orderService.findAllByOrderId(id);
        orderService.deleteOrderItems(orderItemList);
        orderService.delete(order);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
