package com.example.food.controller;


import com.example.food.config.TokenUtils;
import com.example.food.dto.request.LoginRequest;
import com.example.food.dto.request.SignupRequest;
import com.example.food.dto.response.UserDto;
import com.example.food.controller.support.SigninRequestToUser;
import com.example.food.repository.model.Cart;
import com.example.food.repository.model.User;
import com.example.food.service.CartService;
import com.example.food.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private SigninRequestToUser signinRequestToUser;

    @Autowired
    CartService cartService;

    @PreAuthorize("permitAll()")
    @PostMapping("/signup")
    public ResponseEntity<UserDto> signup(@RequestBody @Validated SignupRequest signupRequest) {

        User user = userService.findbyUsername(signupRequest.getUsername());
        if (user != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        user = signinRequestToUser.convert(signupRequest);
        userService.save(user);
        cartService.save(new Cart(user));
        UserDto userDto = new UserDto(user.getId(), user.getUsername(), user.getEmail(), user.getPassword(), user.getRole());
        return new ResponseEntity<>(userDto, HttpStatus.CREATED);
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/signin")
    public ResponseEntity authenticateUser(@RequestBody LoginRequest loginRequest) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        try {
            UserDetails userDetails = userDetailsService.loadUserByUsername(loginRequest.getUsername());
            return ResponseEntity.ok(tokenUtils.generateToken(userDetails));
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
